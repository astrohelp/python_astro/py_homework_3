﻿namespace Life
{
    partial class LifeUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.controlsUI = new System.Windows.Forms.GroupBox();
            this.generationUI = new System.Windows.Forms.TextBox();
            this.clearUI = new System.Windows.Forms.Button();
            this.stopUI = new System.Windows.Forms.Button();
            this.startUI = new System.Windows.Forms.Button();
            this.gridUI = new System.Windows.Forms.GroupBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.controlsUI.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlsUI
            // 
            this.controlsUI.Controls.Add(this.generationUI);
            this.controlsUI.Controls.Add(this.clearUI);
            this.controlsUI.Controls.Add(this.stopUI);
            this.controlsUI.Controls.Add(this.startUI);
            this.controlsUI.Location = new System.Drawing.Point(16, 15);
            this.controlsUI.Margin = new System.Windows.Forms.Padding(4);
            this.controlsUI.Name = "controlsUI";
            this.controlsUI.Padding = new System.Windows.Forms.Padding(4);
            this.controlsUI.Size = new System.Drawing.Size(600, 62);
            this.controlsUI.TabIndex = 0;
            this.controlsUI.TabStop = false;
            // 
            // generationUI
            // 
            this.generationUI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.generationUI.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.generationUI.Font = new System.Drawing.Font("Segoe MDL2 Assets", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generationUI.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.generationUI.Location = new System.Drawing.Point(465, 8);
            this.generationUI.Margin = new System.Windows.Forms.Padding(4);
            this.generationUI.Name = "generationUI";
            this.generationUI.ReadOnly = true;
            this.generationUI.Size = new System.Drawing.Size(127, 37);
            this.generationUI.TabIndex = 4;
            this.generationUI.Text = "0";
            this.generationUI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.generationUI.TextChanged += new System.EventHandler(this.generationUI_TextChanged);
            // 
            // clearUI
            // 
            this.clearUI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.clearUI.FlatAppearance.BorderSize = 0;
            this.clearUI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.clearUI.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clearUI.Location = new System.Drawing.Point(291, 12);
            this.clearUI.Margin = new System.Windows.Forms.Padding(4);
            this.clearUI.Name = "clearUI";
            this.clearUI.Size = new System.Drawing.Size(133, 42);
            this.clearUI.TabIndex = 2;
            this.clearUI.Text = "Clear";
            this.clearUI.UseVisualStyleBackColor = false;
            this.clearUI.Click += new System.EventHandler(this.clearUI_Click);
            // 
            // stopUI
            // 
            this.stopUI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.stopUI.FlatAppearance.BorderSize = 0;
            this.stopUI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.stopUI.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopUI.Location = new System.Drawing.Point(149, 12);
            this.stopUI.Margin = new System.Windows.Forms.Padding(4);
            this.stopUI.Name = "stopUI";
            this.stopUI.Size = new System.Drawing.Size(133, 42);
            this.stopUI.TabIndex = 1;
            this.stopUI.Text = "Stop";
            this.stopUI.UseVisualStyleBackColor = false;
            this.stopUI.Click += new System.EventHandler(this.stopUI_Click);
            // 
            // startUI
            // 
            this.startUI.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.startUI.FlatAppearance.BorderSize = 0;
            this.startUI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startUI.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startUI.Location = new System.Drawing.Point(8, 12);
            this.startUI.Margin = new System.Windows.Forms.Padding(4);
            this.startUI.Name = "startUI";
            this.startUI.Size = new System.Drawing.Size(133, 42);
            this.startUI.TabIndex = 0;
            this.startUI.Text = "Start";
            this.startUI.UseVisualStyleBackColor = false;
            this.startUI.Click += new System.EventHandler(this.startUI_Click);
            // 
            // gridUI
            // 
            this.gridUI.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gridUI.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gridUI.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridUI.Location = new System.Drawing.Point(16, 84);
            this.gridUI.Margin = new System.Windows.Forms.Padding(4);
            this.gridUI.Name = "gridUI";
            this.gridUI.Padding = new System.Windows.Forms.Padding(4);
            this.gridUI.Size = new System.Drawing.Size(600, 554);
            this.gridUI.TabIndex = 1;
            this.gridUI.TabStop = false;
            // 
            // timer
            // 
            this.timer.Interval = 500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // LifeUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.ClientSize = new System.Drawing.Size(629, 644);
            this.Controls.Add(this.gridUI);
            this.Controls.Add(this.controlsUI);
            this.ForeColor = System.Drawing.SystemColors.ScrollBar;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(647, 691);
            this.MinimumSize = new System.Drawing.Size(647, 691);
            this.Name = "LifeUI";
            this.Text = "Conway\'s Game of Life";
            this.Load += new System.EventHandler(this.LifeUI_Load);
            this.controlsUI.ResumeLayout(false);
            this.controlsUI.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox controlsUI;
        private System.Windows.Forms.GroupBox gridUI;
        private System.Windows.Forms.Button clearUI;
        private System.Windows.Forms.Button stopUI;
        private System.Windows.Forms.Button startUI;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.TextBox generationUI;
    }
}

