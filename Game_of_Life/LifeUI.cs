﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace Life
{
    public partial class LifeUI : Form
    {
        private const int GRID_BUTTON_DIMENSION_PX = 25;
        private const int GRID_DIMENSION_PX = 500;
        private const int GRID_DIMENSION_CELLS = GRID_DIMENSION_PX / GRID_BUTTON_DIMENSION_PX;

        private static readonly Color DEAD_CELL_COLOUR = Color.FromArgb(30,30,30);

        private Life engine = null;
        
        public LifeUI()
        {
            InitializeComponent();
            engine = new Life(GRID_DIMENSION_CELLS, GRID_DIMENSION_CELLS);
        }
        
        private void LifeUI_Load(object sender, EventArgs e)
        {
            
            for (int j = 0; j + GRID_BUTTON_DIMENSION_PX <= GRID_DIMENSION_PX; j += GRID_BUTTON_DIMENSION_PX)
                for (int i = 0; i + GRID_BUTTON_DIMENSION_PX <= GRID_DIMENSION_PX; i += GRID_BUTTON_DIMENSION_PX)
                {
                    Button newButton = new Button();
                    newButton.FlatStyle = FlatStyle.Flat;
                    newButton.FlatAppearance.BorderColor = Color.FromArgb(35, 35, 35);
                    newButton.Size = new Size(GRID_BUTTON_DIMENSION_PX, GRID_BUTTON_DIMENSION_PX);
                    newButton.Location = new Point(i+50, j+30);
                    newButton.Click += new EventHandler(ClickCell);
                    gridUI.Controls.Add(newButton);
                }

            UpdateColours();
        }
        
        private void timer_Tick(object sender, EventArgs e)
        {
            engine.Tick();
            generationUI.Text = engine.Ticks.ToString();
            UpdateColours();
        }
        
        public void startUI_Click(object sender, EventArgs e)
        {
            timer.Enabled = true;
            startUI.Enabled = false;
            stopUI.Enabled = true;
        }
        
        private void stopUI_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            stopUI.Enabled = false;
            startUI.Enabled = true;
        }
        
        private void clearUI_Click(object sender, EventArgs e)
        {
            timer.Enabled = false;
            stopUI.Enabled = false;
            startUI.Enabled = true;

            engine = new Life(engine.Height, engine.Width);
            generationUI.Text = engine.Ticks.ToString();

            UpdateColours();
        }
        
        private void ClickCell(object sender, EventArgs e)
        {
            if (timer.Enabled)
                return;

            int buttonLinearIndex = gridUI.Controls.IndexOf(sender as Control);
            int y = buttonLinearIndex / engine.Width;
            int x = buttonLinearIndex % engine.Width;
            
            engine[y, x] = !engine[y, x];
            ((Button)sender).BackColor =  engine[y, x] ? GetLiveColor() : DEAD_CELL_COLOUR;
        }

        private void UpdateColours()
        {
            for (int linearIndex = 0; linearIndex < gridUI.Controls.Count; ++linearIndex)
            {
                var LIVE_CELL_COLOR = GetLiveColor();
                gridUI.Controls[linearIndex].BackColor =
                    engine[linearIndex / engine.Width, linearIndex % engine.Width] ? LIVE_CELL_COLOR : DEAD_CELL_COLOUR;

                //забавный момент: CLR оптимизирует код так, что без этого блока он перестает работать:
                LIVE_CELL_COLOR = Color.Wheat;
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }

        private static Color GetLiveColor()
        {
            var rnd = new Random();
            var clr = rnd.Next(0,8);

            switch(clr)
            {
                case 0:
                    return Color.FromArgb(0, 202, 255);
                case 1:
                    return Color.FromArgb(255, 180, 0);
                case 2:
                    return Color.FromArgb(255, 0, 75);
                case 3:
                    return Color.FromArgb(255, 53, 0);
                case 4:
                    return Color.FromArgb(0, 75, 255);
                case 5:
                    return Color.FromArgb(255, 0, 202);
                case 6:
                    return Color.FromArgb(202, 255, 0);
                case 7:
                    return Color.FromArgb(0, 255, 53);
                default:
                    return Color.White;
            }
        }

        private void generationUI_TextChanged(object sender, EventArgs e)
        {

        }
    }
}