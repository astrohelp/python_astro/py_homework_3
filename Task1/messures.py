#внимание! тут нарушен принцип don't repeat yourself, лучше выделить одну функцию замера времени, а тип перемножения передавать параметром

import numpy as np
import random as rd
import timeit
from statistics import median
from multi import *

#заполняем массив случайными числами
def fill_data(N, n):
    if (N == 1):
        data = np.random.sample(n)
    elif (N == 2):
        data = np.random.sample((n, n))
    else:
        data = np.random.sample((n, n, n))
    return data

#замер времени выполнения вложенных for
def messure_for(N):
    y_for = []; y_np = []; err_for = []; err_np = []
    print(N)
    n_list = list(range(1, 101))
    for n in n_list:
        data1 = fill_data(N, n)
        data2 = fill_data(N, n)
        times = []

        for i in range(10):
            times.append(for_multiplication(N, data1, data2)) #эта штука возвращает время своей работы
        
        y = median(times); #находим медиану 
        err = (max(times)-y)/2 #и наибольшее отклонение от нее
        y_for.append(y)
        err_for.append(err)
    n_list = [i ** N for i in n_list]
    return (n_list, y_for, err_for)

def messure_np(N):
    y_for = []; y_np = []; err_for = []; err_np = []
    print(N)
    n_list = list(range(1, 101))
    for n in n_list:
        data1 = fill_data(N, n)
        data2 = fill_data(N, n)
        times = []

        for i in range(10):
            times.append(matrix_multiplication(data1, data2)) #эта штука тоже возвращает время
        
        y = median(times);
        err = (max(times)-y)/2
        y_np.append(y)
        err_np.append(err)
    n_list = [i ** N for i in n_list]
    return (n_list, y_np, err_np)

def messure_for_zip(N):
    y_for = []; y_np = []; err_for = []; err_np = []
    print(N)
    n_list = list(range(1, 101))
    for n in n_list:
        data1 = fill_data(N, n)
        data2 = fill_data(N, n)
        times = []

        for i in range(10):
            times.append(for_zip_multiplication(N, data1, data2)) #и эта
        
        y = median(times);
        err = (max(times)-y)/2
        y_for.append(y)
        err_for.append(err)
    n_list = [i ** N for i in n_list]
    return (n_list, y_for, err_for)