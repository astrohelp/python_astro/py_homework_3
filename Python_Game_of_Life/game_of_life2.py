from tkinter import Tk
from tkinter import Canvas
from tkinter import Button
import random
import time
###################################################################################################################################

GRID_WIDTH = 10
GRID_HEIGHT = 10
CELL_SIDE_LENGTH = 40

root = Tk()                                 #создаем окно ткинтера
root.title = "Game"
root.resizable(0,0)							#запрещаем ресайз
root.wm_attributes("-topmost", 1)			#выставляем аттрибут topmost в 1, чтобы окно было поверх всех других окон

canvas = Canvas(root, width=GRID_WIDTH * CELL_SIDE_LENGTH, height=GRID_HEIGHT * CELL_SIDE_LENGTH, bd=0, highlightthickness=0)        #в этом окне создаем канвас - поверхность, на которой можно рисовать. Первым аргументом передаем родителя - окно
canvas.pack()								#отображаем канвас, используя пакер (ткнитеровский метод, после его вызова, элемент на котором он вызван, показывается в окне)

###################################################################################################################################

class Grid:                                                                 #класс сетки - хранит двумерный массив с клетками и предоставляет методы

    #  Initialization function (all the precalled things)
    def __init__(self, height, width):										#metod __init__ вызывается после создания объекта: сохраняем размеры сетки, создаем массив клеток, методом Clear заполняем все клетки значением False
        self.cells = {}
        self.height = height
        self.width = width
        self.Clear()
                
    def SetCell(self, x, y, isAlive):										# методы SetCell и GetCell позволяют положить значение в клетку по координатам и взять его
        self.cells[(x,y)] = isAlive
        
    def GetCell(self, x, y):
        return self.cells[(x,y)]
        
    def CheckIfAlive(self, x, y):											# метод CheckIfAlive - проверяет для клетки по координатам, выживет ли она в следующем поколении
        neighboursCounter = 0
        for i in range(max(0, x - 1), min(x + 2, self.height)):
            for j in range(max(0, y - 1), min(y + 2, self.width)):
                if (self.GetCell(i, j)):
                    neighboursCounter += 1
        if (self.GetCell(x, y)):
            neighboursCounter -= 1
            return neighboursCounter == 3 or neighboursCounter == 2
        return neighboursCounter == 3

    def Clear(self):														# для каждой клетки вызываем SetCell
        for x in range(0, self.height):
            for y in range(0, self.width):
                self.SetCell(x, y, False)\

class Vector2:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y

class GridView:																	#главный класс, отображающий сетку
    def __init__(self, canvas, cellLength, bgColor, cellColor, grid):			#в конструкторе сохраняем параметры
        self.canvas = canvas
        self.squares = {}
        self.cellLength = cellLength
        self.cellColor = cellColor
        self.bgColor = bgColor
        self.grid = grid
        self.isRunning = False        



		# Как это работает:
		# В этом конструкторе мы создаем Х х У квадратов, которые потом раз в пять миллисекунд будем обновлять каждый нужным цветом
		# в зависимости от значений в массиве класса Grid,  который писали выше

        for x in range(0, self.grid.height):									#создаем массив квадратов на канвасе 
            for y in range(0, self.grid.width):					
                self.squares[(x,y)] = self.CreateCell(x, y, False)					# сохраняем их айдишники, они пригодятся, чтобы их обновлять
					
        self.canvas.bind("<Button-1>", self.canvas_onclick)					#биндаем вызов метода canvas_onclick на нажатие левой кнопки мыши

    def CreateCell(self, x, y, isAlive):										# создает квадрат нужного размера по нужным координатам
        topLeft = Vector2(x * self.cellLength, y * self.cellLength)
        downRight = Vector2(topLeft.x + self.cellLength, topLeft.y + self.cellLength)
        color = self.GetColor(isAlive)
        return self.canvas.create_rectangle(topLeft.x, topLeft.y, downRight.x, downRight.y, fill=color)  

    def GetColor(self, isAlive):
        if (isAlive):
            return self.cellColor
        else:
            return self.bgColor

    def canvas_onclick(self, event):										# по клику вычисляем в какую клетку мы попали и меняем её значение на противоположное
        xCellInd = event.x // self.cellLength
        yCellInd = event.y // self.cellLength
        self.grid.SetCell(xCellInd, yCellInd, not self.grid.GetCell(xCellInd, yCellInd))

    def draw(self):																# основной цикл отрисовки
        if(self.isRunning):
            self.Tick()															#если включена симуляция - пересчитываем сетку

        for x in range(0, self.grid.height):
            for y in range(0, self.grid.width): 
				# метод itemconfig первым аргументом принимает айдишник элемента, конфиги которого надо менять - передаем ранее сохраненные айдишники квадратов
				# меняем им параметр fill 
                self.canvas.itemconfig(self.squares[(x,y)], fill=self.GetColor(self.grid.GetCell(x,y)))    #обновляем каждый квадрат нужным цветом
        self.canvas.after(50, self.draw)        

    def Tick(self):																		# создаем новую сетку, для каждой клетки проверяем, останется ли она жива
        self.newGrid = Grid(self.grid.width, self.grid.height)
        for x in range(0, self.grid.height):
            for y in range(0, self.grid.width):
                self.newGrid.SetCell(x, y, self.grid.CheckIfAlive(x, y))
        self.grid = self.newGrid;													# после проверок кладем новую сетку вместо старой

###################################################################################################################################

goButton = Button(root, text="go")				# создаем кнопку

def on_button_click():													# по нажатии на гуишную кнопку меняем флаг IsRunning и текст на кнопке
        gridView.isRunning = not gridView.isRunning
        if gridView.isRunning:
            buttonText = "stop"
        else:
            buttonText = "go"
        goButton.configure(text=buttonText)

grid = Grid(GRID_WIDTH, GRID_HEIGHT)											#создаем сетку								
gridView = GridView(canvas, CELL_SIDE_LENGTH, "white", "blue", grid)			#создаем 
goButton.configure(command=on_button_click)										#биндим метод on_button_click на нажатие кнопки
goButton.pack()																	#отображаем кнопку пакером
gridView.draw()  
root.mainloop()



#Grid - по сути просто двумерный массив булевых значений с методами доступа к нему
#А GridView занимается его (Grid) отображением на канвас